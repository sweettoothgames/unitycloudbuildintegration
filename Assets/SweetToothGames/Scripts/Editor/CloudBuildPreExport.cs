﻿/*
 * In Unity cloud build Config you need to set the PreExport Method to: 
 * SweetToothGames.CloudBuildPreExport.PreExport 
 * 
 * Under Environment Variables you then need to add the following two variables
 * CCDBucket
 * CCDBadge
 * 
 */
#if UNITY_CLOUD_BUILD



using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Linq;

namespace SweetToothGames
{
    public class CloudBuildPreExport
    {
        public static void PreExport(UnityEngine.CloudBuild.BuildManifestObject manifest)
        {

            string buildNumberX = manifest.GetValue<string>("buildNumber");
            int buildNumberInt = System.Convert.ToInt32(buildNumberX);
            
            Debug.Log("[CloudBuildPreExport] Build Number=" + buildNumberX + ", int=" + buildNumberInt);

#if UNITY_IOS
        Debug.Log("[CloudBuildPreExport] Set IOS 'buildNumber' to:" + buildNumberX);
        PlayerSettings.iOS.buildNumber = buildNumberX;
#endif

#if UNITY_ANDROID
        Debug.Log("[CloudBuildPreExport] Set ANDROID 'bundleVersionCode' to:" + buildNumberInt);
        PlayerSettings.Android.bundleVersionCode = buildNumberInt;
#endif

#if KINDLE
        PlayerSettings.Android.targetArchitectures = AndroidArchitecture.All;
#endif

            var ccdBucket = Environment.GetEnvironmentVariable("CCDBucket");
            var ccdBadge = Environment.GetEnvironmentVariable("CCDBadge");

            Debug.Log($"[CloudBuildPreExport] PreExport CCD Bucket: {ccdBucket}");
            Debug.Log($"[CloudBuildPreExport] PreExport CCD Badge: {ccdBadge}");

            var addressableConfig = AddressableConfig.Instance();

            if (addressableConfig != null)
            {
                addressableConfig.Init(ccdBucket, ccdBadge);
            }
            else
            {
                AddressableConfig config = ScriptableObject.CreateInstance<AddressableConfig>();
                config.Init(ccdBucket, ccdBadge);
                AssetDatabase.CreateAsset(config, AddressableConfig.filePath);
            }

            AssetDatabase.SaveAssets();

        }
    }
}
#endif
