﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


namespace SweetToothGames
{
	public abstract class AbstractAddressableConfig : ScriptableObject { }

	[CreateAssetMenu(menuName = "Addressable Config", order = 0)]
	public class AddressableConfig : AbstractAddressableConfig
	{
		
		// URL Example Production
		// https://[CloudContentDeliveryAccountID].client-api.unity3dusercontent.com/client_api/v1/buckets/{AddressableConfig.CCDBucket}/release_by_badge/{AddressableConfig.CCDBadge}/entry_by_path/content/?path=ServerData/[BuildTarget]


		public const string fileName = "AddressableConfig";
		public const string filePath = "Assets/Resources/AddressableConfig.asset";


#if Unity_IOS
	private static string Default_CCDBucket = ""; // IOS Development
	private static string Default_CCDBadge = "latest";
#elif Unity_Android
	private static string Default_CCDBucket = ""; // Android Development
	private static string Default_CCDBadge = "latest";
#else
		private static string Default_CCDBucket = ""; // IOS Development
		private static string Default_CCDBadge = "latest";
#endif

		
		private static AddressableConfig s_instance;

		public static AddressableConfig Instance()
		{
			if (s_instance != null) return s_instance;

#if UNITY_EDITOR
			s_instance = AssetDatabase.LoadAssetAtPath<AddressableConfig>(filePath);
			if (s_instance != null)
			{
				Debug.Log("[AddressableConfig] Loaded using AssetDatabase");
				return s_instance;
			}
#endif

			s_instance = Resources.Load<AddressableConfig>(AddressableConfig.fileName);
			if (s_instance != null)
			{
				Debug.Log($"[AddressableConfig] Loaded using Resources.Load<AddressableConfig>({AddressableConfig.fileName})");
				return s_instance;
			}

			s_instance = Resources.Load<AddressableConfig>($"{AddressableConfig.fileName}.asset");
			if (s_instance != null)
			{
				Debug.Log($"[AddressableConfig] Loaded using Resources.Load<AddressableConfig>({AddressableConfig.fileName}.asset)");
				return s_instance;
			}

			var instance = Resources.FindObjectsOfTypeAll<AddressableConfig>();
			if (instance != null && instance.Length > 0)
			{
				Debug.Log($"[AddressableConfig] Loaded using Resources.FindObjectsOfTypeAll<AddressableConfig>()");
				s_instance = instance[0];
			}

			return s_instance;
		}

		public static string CCDBucket
		{
			get
			{
				var instance = Instance();
				if (instance == null) return Default_CCDBucket;
				return instance.m_CCDBucket;
			}
		}

		public static string CCDBadge
		{
			get
			{
				var instance = Instance();
				if (instance == null) return Default_CCDBadge;
				return instance.m_CCDBadge;
			}
		}

		public static bool UpdatedInBuild
		{
			get
			{
				var instance = Instance();
				if (instance == null) return false;
				return instance.m_UpdatedInBuild;
			}
		}




		public void Init(string bucket, string badge)
		{
			m_CCDBucket = bucket;
			m_CCDBadge = badge;

#if UNITY_CLOUD_BUILD
			m_UpdatedInBuild = true;
#endif
		}

		[SerializeField] private string m_CCDBucket = Default_CCDBucket;
		[SerializeField] private string m_CCDBadge = Default_CCDBadge;
		private bool m_UpdatedInBuild = false;


	}
}