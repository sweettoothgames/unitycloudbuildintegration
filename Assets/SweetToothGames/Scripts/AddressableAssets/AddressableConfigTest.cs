﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SweetToothGames
{
    public class AddressableConfigTest : MonoBehaviour
    {
        void Start()
        {
            Debug.Log($"[AddressableConfigTest] Updated: {AddressableConfig.UpdatedInBuild}");
            Debug.Log($"[AddressableConfigTest] CCD Bucket: {AddressableConfig.CCDBucket}");
            Debug.Log($"[AddressableConfigTest] CCD Badge: {AddressableConfig.CCDBadge}");
        }
    }
}